package com.java.section2;

import java.util.Scanner;

public class Application2 {

	public static void main(String[] args) {
		
		/* 주민등록번호를 스캐너로 입력 받고 문자 배열로 저장한 뒤,
		 * 성별 자리 이후부터 *로 가려서 출력하세요
		 * 
		 * -- 입력 예시 --
		 * 주민등록번호를 입력하세요 : 990101-1234567
		 * 
		 * -- 출력 예시 --
		 * 990101-1******
		 */
		Scanner sc = new Scanner(System.in);
		while(true) {
			String regExp = "^([0-9]{2}(0[1-9]|1[0-2])(0[1-9]|[1,2][0-9]|3[0,1]))-[1-4][0-9]{6}$";
			System.out.print("주민등록번호를 입력하세요 : ");
			String str = sc.next();
			System.out.println(str.matches(regExp));
			char[] arr = new char[14];
			if(str.matches(regExp)) {
				for(int i=0; i<str.length(); i++) {
					arr[i] = str.charAt(i);
				}
				for(int j=8; j<arr.length; j++) {
					arr[j] = '*';
				}
				for(int k=0; k<arr.length; k++) {
					System.out.print(arr[k]);
				}
				break;
			}
		}
	}
}
